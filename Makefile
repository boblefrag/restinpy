DOCKER_NAME = registry.gitlab.com/lisael/restinpy/python3

docker_build_dev:
	cp ./dev_requirements.txt docker/pythontest/dev_requirements.txt
	sudo docker build -t $(DOCKER_NAME):latest docker/pythontest/
	sudo docker push $(DOCKER_NAME)

coverage:
	pytest --cov=restinpy --cov-report=html --cov-report=term -v -s --full-trace tests

.PHONY: docker_build_dev coverage
