from restinpy.parser import YamlLoader


class ValidatorBase(type):
    registry = {}

    def __init__(cls, name, bases, attrs):
        if "__validator_name__" in attrs:
            ValidatorBase.registry[cls.__validator_name__] = cls


class Validator(YamlLoader, metaclass=ValidatorBase):
    async def __call__(self, response, ctx):
        raise NotImplementedError()


def validator_factory(data):
    validator_name, data = list(data.items())[0]
    if isinstance(data, list):
        return ValidatorBase.registry[validator_name](data)
    else:
        return ValidatorBase.registry[validator_name](**data)


class ValidationError(Exception):
    pass
