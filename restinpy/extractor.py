from restinpy.parser import YamlLoader


class ExtractorBase(type):
    registry = {}

    def __init__(cls, name, bases, attrs):
        if "__extractor_name__" in attrs:
            ExtractorBase.registry[cls.__extractor_name__] = cls


class Extractor(YamlLoader, metaclass=ExtractorBase):
    async def __call__(self, response, ctx):
        raise NotImplementedError()


def extractor_factory(data):
    extractor_name, data = list(data.items())[0]
    try:
        return ExtractorBase.registry[extractor_name](**data)
    except KeyError:
        raise ValueError("No extractor named `%s` found" % extractor_name)
