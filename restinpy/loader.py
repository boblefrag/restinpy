import yaml
import os

from restinpy.context import Context
from restinpy.suite import Suite

async def load_context(name):
    "load a context from its name"
    base_dir = os.path.join(os.curdir, "context")
    path = os.path.join(base_dir, name + ".yml")
    with open(path, 'r') as f:
        ctx = Context(f)
    extends = ctx.get("extends")
    if extends is not None:
        path = os.path.join(base_dir, extends)
        with open(path) as f:
            overlay = ctx
            ctx = Context(f)
            ctx.update(overlay)
    return ctx


async def load_suite(name):
    path = os.path.join("suite", name + ".yml")
    with open(path, 'r') as f:
        data = yaml.load(f)
    s = Suite(data)
    s.__file__ = path
    return s
