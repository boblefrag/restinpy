from io import StringIO

UNDEF = object()


class YamlLoader:
    def __init__(self, *args, **kwargs):
        self.__fields__ = {}
        for k, v in kwargs.items():
            setattr(self, k, v)

    def yl_update(self, other):
        self.__fields__.update(other)


class YamlField:
    ord_gen = iter(range(99999999))

    def __init__(self, help=None, default=UNDEF, factory=None):
        self.help = help
        self.factory = factory
        self.default = default if not factory else None
        self.ord = next(self.ord_gen)

    def __get__(self, instance, owner):
        if instance is None:
            return self
        try:
            return instance.__fields__[self.name]
        except KeyError:
            if self.default is UNDEF:
                raise AttributeError(self.name)
            if self.factory is not None:
                val = self.factory()
            else:
                val = self.default
            instance.__fields__[self.name] = val
            return val

    def __set__(self, instance, value):
        instance.__fields__[self.name] = value

    def __delete__(self, instance):
        del instance.__fields__[self.name]

    def __set_name__(self, owner, name):
        self.name = name


class ObjField(YamlField):
    def __init__(self, klass, *args, **kwargs):
        self.klass = klass
        super().__init__(*args, **kwargs)

    def __set__(self, instance, value):
        instance.__fields__[self.name] = self.klass(**value)


class ChoiceField(YamlField):
    pass


class BooleanField(YamlField):
    pass


class StringField(YamlField):
    pass


class MappingField(YamlField):
    def __get__(self, instance, owner):
        try:
            return instance.__fields__[self.name]
        except KeyError:
            return {}


class ListField(YamlField):
    def __init__(self, item_type=None, **kwargs):
        super().__init__(**kwargs)
        self.item_type = item_type

    def __set__(self, instance, value):
        if not isinstance(value, list):
            raise ValueError(
                "{} expects a yaml list, got {}".format(
                    type(self), type(value)))
        instance.__fields__[self.name] = value

    def __get__(self, instance, owner):
        val = super().__get__(instance, owner)
        if self.item_type is None:
            return val
        return [self.item_type(i) for i in val]


class YamlLoaderVisitor:

    def extract_fields(self, loader):
        cls = type(loader)
        fields = [
                (name, field, getattr(loader, name))
                for (name, field) in cls.__dict__.items()
                if isinstance(field, YamlField)]
        fields.sort(key=lambda x: x[1].ord)
        return fields

    def visit(self, loader):
        fields = self.extract_fields(loader)
        return [(name, self.visit_field(field, val)) for (name, field, val) in fields]

    def visit_field(self, field, val):
        cls = type(field)
        # get the first visitable class in field's __mro__
        for parent in cls.__mro__:
            name = "visit_%s" % parent.__name__.lower()
            if hasattr(self, name):
                return getattr(self, name)(field, val)
        raise ValueError(field)

    def visit_objfield(self, field, val):
        return self.__class__().visit(val)

    def visit_stringfield(self, field, val):
        return self.visit_generic(field, val)

    def visit_choicefield(self, field, val):
        return self.visit_generic(field, val)

    def visit_mappingfield(self, field, val):
        return self.visit_generic(field, val)

    def visit_listfield(self, field, val):
        return self.visit_generic(field, val)

    def visit_booleanfield(self, field, val):
        return self.visit_generic(field, val)

    def visit_generic(self, field, val):
        raise NotImplementedError()


class YamlLoaderRenderer(YamlLoaderVisitor):
    def __init__(self, indent=0, output=None, comments=False):
        self.output = StringIO() if output is None else output
        self._indent = indent
        self.comments = comments

    def indent(self):
        self.output.write("  " * self._indent)

    def writeln(self, txt):
        self.indent()
        self.output.write(txt)
        self.output.write("\n")

    def write_comment(self, field):
        if not self.comments:
            return
        if field.help:
            self.writeln("# %s" % field.help)
        if field.default is not UNDEF:
            self.writeln("# Default: %s" % field.default)

    def visit(self, loader):
        for (name, field, val) in self.extract_fields(loader):
            self.write_comment(field)
            self.indent()
            self.output.write("%s: " % name)
            self.visit_field(field, val)

    def visit_scalar(self, val):
        if isinstance(val, dict):
            return self.visit_dict(val)
        if isinstance(val, list):
            return self.visit_list(val)
        if isinstance(val, str):
            return self.visit_str(val)
        if isinstance(val, bool):
            return self.visit_bool(val)
        if isinstance(val, int):
            return self.visit_int(val)
        if isinstance(val, float):
            return self.visit_float(val)
        if val is None:
            return self.visit_none(val)
        raise ValueError("%s is not a yamltype")

    def visit_str(self, val):
        # TODO: handle multi-line txt
        self.output.write(repr(val))
        self.output.write("\n")

    def visit_int(self, val):
        self.output.write(repr(val))
        self.output.write("\n")

    def visit_none(self, val):
        self.output.write("null\n")

    def visit_bool(self, val):
        val = "yes" if val else "no"
        self.output.write(val)
        self.output.write("\n")

    def visit_objfield(self, field, val):
        self.output.write("\n")
        return self.__class__(
            indent=self._indent+1,
            comments=self.comments,
            output=self.output).visit(val)

    def visit_generic(self, field, val):
        self.visit_scalar(val)

    def visit_mappingfield(self, field, val):
        if not val:
            self.output.write("{}\n")
            return
        self.output.write("\n")
        self._indent += 1
        for k, v in val.items():
            self.indent()
            self.output.write("%s: " % k)
            self.visit_scalar(v)
        self._indent -= 1

    def visit_listfield(self, field, val):
        if not val:
            self.output.write("[]\n")
            return
        self.output.write("\n")
        self._indent += 1
        for item in val:
            self.writeln("- %s" % str(item))
        self._indent -= 1


def yamlloader_as_yaml(loader, comments=False, indent=0, name=None, output=None):
    output = StringIO() if output is None else output
    if name is not None:
        output.write(name + ":\n")
        indent += 1
    renderer = YamlLoaderRenderer(indent=indent, comments=comments, output=output)
    renderer.visit(loader)
    if hasattr(renderer.output, "getvalue"):
        return renderer.output.getvalue()


class YamlVisitor:
    def visit(self,  data):
        if isinstance(data, dict):
            return self.visit_dict(data)
        if isinstance(data, list):
            return self.visit_list(data)
        if isinstance(data, str):
            return self.visit_str(data)
        if isinstance(data, int):
            return self.visit_int(data)
        if isinstance(data, float):
            return self.visit_float(data)
        if data is None:
            return self.visit_none(data)
        return self.visit_generic(data)

    def visit_dict(self, data):
        return {k: self.visit(v) for (k, v) in data.items()}

    def visit_list(self, data):
        return [self.visit(i) for i in data]

    def visit_str(self, data):
        return data

    def visit_int(self, data):
        return data

    def visit_float(self, data):
        return data

    def visit_none(self, data):
        return data

    def visit_generic(self, data):
        raise NotImplementedError(type(data))
