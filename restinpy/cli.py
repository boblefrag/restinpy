# -*- coding: utf-8 -*-

"""Console script for apitest."""
import sys
import asyncio
import aiohttp
import json

from jq import jq

import click

from restinpy.loader import load_context, load_suite
from restinpy import init_plugins


async def run(context, suite, extra_vars, debug, output):
    context = await load_context(context)
    for k, v in extra_vars:
        context.bind(k, v, create=True)
    if output == "stdout":
        out = sys.stdout
    else:
        out = sys.stderr
    context["_out"] = out
    context["_debug"] = debug
    init_plugins()
    suite = await load_suite(suite)
    async with aiohttp.ClientSession() as sess:
        context["_http_session"] = sess
        result = await suite(context)
    result = result.as_dict()
    out.write(format_summary(result) + "\n")
    if output == "json":
        print(json.dumps(result))


def format_summary(result):
    tests = jq(
        '[..|objects|select(.type=="test")]'
    ).transform(result)
    skipped = jq(
        '[..|objects|select(.type=="test")|select(.skipped)]'
    ).transform(result)
    failed = jq(
        '[..|objects|select(.type=="test")|select(.success==false)]'
    ).transform(result)
    success = jq(
        '[..|objects|select(.type=="test")|select(.success==true)]'
    ).transform(result)
    output = "Summary\n"
    summary = "tests: %s    success: %s    skipped: %s    failed: %s\n" % (
        len(tests), len(success), len(skipped), len(failed))
    output += "*" * len(summary) + "\n"
    output += summary
    output += "*" * len(summary) + "\n"
    return output


def parse_extra(extra_vars):
    return [extra.split("=", 1) for extra in extra_vars]


@click.command()
@click.argument("context")
@click.argument("suite")
@click.option("-e", "--extra-var",
              multiple=True,
              metavar="'var.nested=val'",
              help="Set a context value. Multiple --extra-var can be passed.")
@click.option("-d", "--debug",
              is_flag=True, help="Toggle debug output for all tasks.")
@click.option("-o", "--output",
              type=click.Choice(["-", "json"]),
              default="-",
              show_default=True,
              help="Output format. "
              "If not '-' (stdout), all debug output is dumped in stderr.")
def main(context, suite, extra_var, debug, output):
    """Run the given SUITE, starting with the given CONTEXT"""
    extra = parse_extra(extra_var)
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run(context, suite, extra, debug, output))
    return 0


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
