import aiohttp
from asyncio import iscoroutine, ensure_future, wait

from restinpy.task import Task, task_factory


class Suite(Task):

    def __init__(self, data):
        super().__init__(data)
        self._result["tasks"] = []
        self.__file__ = self.__class__.__name__
        self.tasks = []
        for taskdef in data:
            task = task_factory(taskdef)
            self.tasks.append(task)
        self.futures = {}

    async def __run__(self, ctx):
        self._result.taskname = self.__file__
        for task in self.tasks:
            if hasattr(task, "name"):
                name = ctx.render(task.name)
            else:
                name = task.__taskname__
            name = "%s: %s" % (self.__file__, name)
            self._result.log("*" * len(name))
            self._result.log(name)
            self._result.log("*" * len(name))
            result = await task(ctx)
            if iscoroutine(result):
                self.futures[ensure_future(result)] = name
                continue
            else:
                result.taskname = name
                if not self._parse_result(result):
                    break
            self._result.log("")
        if self.futures:
            done, _ = await wait(list(self.futures.keys()))
            if done:
                for d in done:
                    res = d.result()
                    res.taskname = self.futures[d]
                    self.parse_result(res)
        if all([len(r["errors"]) == 0 for r in self._result["tasks"]]):
            self._result.log("OK")
        return self._result

    def _parse_result(self, result):
        self._result["tasks"].append(result.as_dict())
        if not result.success:
            self._result.log_error("task failed")
        return result.success

