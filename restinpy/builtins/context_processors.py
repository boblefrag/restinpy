from restinpy.context_processor import ContextProcessor
from restinpy.parser import MappingField
from .fields import ContextValueField


class Updater(ContextProcessor):
    __context_processor_name__ = "update"
    updates = MappingField()

    def __init__(self, **kwargs):
        self.__fields__ = {}
        self.updates = kwargs

    async def __call__(self, ctx):
        for k, v in self.updates.items():
            ctx.bind(k, ctx.render(v), create=True)


class Copyier(ContextProcessor):
    __context_processor_name__ = "copy"
    src = ContextValueField()
    dest = ContextValueField()
    update = MappingField()

    async def __call__(self, ctx):
        ctx.bind(self.dest, ctx.get_path(self.src))
        for k, v in self.update.items():
            ctx.bind(".".join([self.dest, k]), ctx.render(v), create=True)
