from restinpy.validator import Validator, ValidationError


class StatusValidator(Validator):
    __validator_name__ = "status"

    def __init__(self, expected_statuses):
        self.expected = expected_statuses

    async def __call__(self, response, ctx):
        if response.status not in self.expected:
            raise ValidationError(
                "Response status, expected %s, got %s" % (
                    self.expected, response.status))
