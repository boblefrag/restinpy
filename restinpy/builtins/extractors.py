from jq import jq

from restinpy.parser import StringField
from restinpy.extractor import Extractor

from .fields import ContextValueField


class JQExtractor(Extractor):
    __extractor_name__ = "jq"
    pattern = StringField()
    bind = ContextValueField()

    async def __call__(self, response, ctx):
        data = jq(self.patern).transform(await response.json())
        ctx.bind(self.bind, data, create=True)
