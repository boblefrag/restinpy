import jinja2
import yaml


class Visitor:
    def visit(self, ctx):
        if ctx.is_map():
            return self.visit_map(ctx)
        elif ctx.is_list():
            return self.visit_list(ctx)
        elif ctx.is_scalar():
            return self.visit_scalar(ctx)

    def visit_map(self, ctx):
        return ctx

    def visit_list(self, ctx):
        return ctx

    def visit_scalar(self, ctx):
        return ctx


class YamlVisistor:
    def visit(self, data, ctx):
        if isinstance(data, dict):
            return self.visit_map(data, ctx)
        elif isinstance(data, list):
            return self.visit_list(data, ctx)
        elif isinstance(data, str):
            return self.visit_str(data, ctx)
        elif isinstance(data, bool):
            return self.visit_bool(data, ctx)
        elif isinstance(data, int):
            return self.visit_int(data, ctx)
        elif isinstance(data, float):
            return self.visit_float(data, ctx)

    def visit_map(self, data, ctx):
        return data

    def visit_list(self, data, ctx):
        return data

    def visit_str(self, data, ctx):
        return data

    def visit_bool(self, data, cts):
        return data

    def visit_int(self, data, cts):
        return data

    def visit_float(self, data, cts):
        return data


class ContextBuilder(Visitor):
    def __init__(self, root):
        self.root = root

    def visit_map(self, ctx):
        for k, v in ctx.data.items():
            data = Context.from_data(v, self.root)
            ctx.data[k] = self.visit(data)
        return ctx

    def visit_list(self, ctx):
        for idx, item in enumerate(ctx.data):
            data = Context.from_data(item, self.root)
            ctx.data[idx] = data
        return ctx


class ContextRenderer(Visitor):
    def __init__(self, jinja_env):
        self.jinja_env = jinja_env

    def visit_scalar(self, ctx):
        if isinstance(ctx.data, str):
            try:
                return self.jinja_env.from_string(ctx.data).render(
                        ctx.root.items())
            except UndefinedError as e:
                e.template = ctx.data
                raise
            except Exception:
                return ctx.data
        else:
            return ctx.data

    def visit_map(self, ctx):
        result = {}
        for k, v in ctx.items():
            try:
                rendered = v._render()
            except UndefinedError as e:
                e.path.append(k)
                raise
            result[k] = rendered
        return result

    def visit_list(self, ctx):
        result = []
        for idx, item in enumerate(ctx):
            try:
                rendered = item._render()
            except UndefinedError as e:
                e.path.append(idx)
                raise
            result.append(rendered)
        return result


class ContextUpdater(Visitor):
    def __init__(self, other):
        self.other = other
        self.path = []

    def get_from_path(self):
        data = self.other.data
        for part in self.path:
            data = data[part]
        return data

    def visit_map(self, ctx):
        for k, v in ctx.items():
            self.path.append(k)
            try:
                ctx.data[k] = self.visit(self.get_from_path())
            except Exception:
                pass
            self.path.pop()
        other = self.get_from_path()
        for k, v in other.items():
            if k not in ctx.data:
                ctx[k] = v
        return ctx

    def visit_list(self, ctx):
        for idx, item in enumerate(ctx):
            self.path.append(idx)
            try:
                ctx.data[idx] = self.visit(self.get_from_path())
            except Exception:
                pass
            self.path.pop()
        return ctx


class UndefinedError(Exception):
    def __init__(self, default, help, fallback):
        self.default = default
        self.help = help
        self.fallback = fallback

        self.path = []
        self.template = None

    def __str__(self):
        return ("`{}` is not defined.\nhelp: {}\n" +
                "default: `{}`\ntemplate was: `{}`").format(
            ".".join(reversed(self.path)),
            self.help,
            self.default,
            self.template)


class UndefinedFilter():
    def __init__(self, value, help=None, fallback=True):
        self.fallback = fallback
        self.help = help
        self.default = value

    def __str__(self):
        if self.fallback:
            return str(self.default)
        else:
            raise UndefinedError(self.default, self.help, self.fallback)


class Context:
    def __init__(self, src, root=None):
        self.root = root if root else self
        self._env = None
        if src is not None:
            self.data = yaml.load(src)
            ContextBuilder(self.root).visit(self)
        else:
            self.data = None

    @property
    def env(self):
        if self.root is None or self.root is self:
            if self._env is None:
                self._env = jinja2.Environment()
                self._env.filters['undefined'] = UndefinedFilter
            return self._env
        else:
            return self.root.env

    def __iter__(self):
        return iter(self.data)

    def __str__(self):
        return self._render()

    def items(self):
        return self.data.items()

    def __setitem__(self, key, value):
        if self.data is None:
            self.data = {}
        self.data[key] = Context.from_data(value, self.root)

    def __getitem__(self, key):
        data = self.data[key]
        if isinstance(data, Context):
            return data._render()
        return data

    def get(self, key, default=None):
        if self.is_map():
            if key in self.data:
                return self.__getitem__(key)
        return default

    def _render(self):
        return ContextRenderer(self.env).visit(self)

    def render(self, template):
        if isinstance(template, dict):
            return {self.render(k): self.render(v)
                    for (k, v) in template.items()}
        if isinstance(template, list):
            return [self.render(i) for i in template]
        if isinstance(template, str):
            return self.env.from_string(template).render(self.root._render())
        else:
            return template

    def bind(self, path, value, create=False):
        try:
            key, rest = path.split(".", 1)
        except ValueError:
            self[path] = value
        else:
            try:
                self.data[key].bind(rest, value, create=create)
            except KeyError:
                if create:
                    self[key] = {}
                    self.data[key].bind(rest, value, create=create)
                else:
                    raise

    def get_path(self, path):
        try:
            key, rest = path.split(".", 1)
        except ValueError:
            return self[path]
        else:
            return self.data[key].get_path(rest)

    def is_map(self):
        return isinstance(self.data, dict)

    def is_list(self):
        return isinstance(self.data, list)

    def is_scalar(self):
        return not (isinstance(self.data, dict) or isinstance(self.data, dict))

    def update(self, other):
        other = Context.from_data(other, self.root)
        ContextUpdater(other).visit(self)

    @classmethod
    def from_data(cls, data, root):
        if isinstance(data, cls):
            data.root = root
            return data
        ctx = cls(None, root=root)
        ctx.data = data
        ContextBuilder(root).visit(ctx)
        return ctx


def context_from_file(file):
    return Context(file)
