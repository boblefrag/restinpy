import os
from subprocess import run
import cmd
from tempfile import mkstemp
import asyncio

import pyaml

from restinpy.parser import yamlloader_as_yaml
from pprint import pprint


class TaskShell(cmd.Cmd):
    def __init__(self, task, context, *args, **kwargs):
        self.task = task
        self.ctx = context
        super().__init__(*args, **kwargs)

    async def cmdloop(self, intro=None):
        """
        Near-vebatim copy of cmd.Cmd.cmdloop() (modulo 1 await) to allow
        calls in commands.
        """

        self.preloop()
        if self.use_rawinput and self.completekey:
            try:
                import readline
                self.old_completer = readline.get_completer()
                readline.set_completer(self.complete)
                readline.parse_and_bind(self.completekey+": complete")
            except ImportError:
                pass
        try:
            if intro is not None:
                self.intro = intro
            if self.intro:
                self.stdout.write(str(self.intro)+"\n")
            stop = None
            while not stop:
                if self.cmdqueue:
                    line = self.cmdqueue.pop(0)
                else:
                    if self.use_rawinput:
                        try:
                            line = input(self.prompt)
                        except EOFError:
                            line = 'EOF'
                    else:
                        self.stdout.write(self.prompt)
                        self.stdout.flush()
                        line = self.stdin.readline()
                        if not len(line):
                            line = 'EOF'
                        else:
                            line = line.rstrip('\r\n')
                line = self.precmd(line)
                stop = await self.onecmd(line)
                stop = self.postcmd(stop, line)
            self.postloop()
        finally:
            if self.use_rawinput and self.completekey:
                try:
                    import readline
                    readline.set_completer(self.old_completer)
                except ImportError:
                    pass

    async def onecmd(self, line):
        """
        Near verbatim copy of cmd.Cmd.onecmd
        """
        cmd, arg, line = self.parseline(line)
        if not line:
            return self.emptyline()
        if cmd is None:
            return self.default(line)
        self.lastcmd = line
        if line == 'EOF' :
            self.lastcmd = ''
        if cmd == '':
            return self.default(line)
        else:
            try:
                func = getattr(self, 'do_' + cmd)
            except AttributeError:
                return self.default(line)
            if asyncio.iscoroutinefunction(func):
                return await func(arg)
            return func(arg)

    @property
    def prompt(self):
        return self.task.name + " > "

    def do_context(self, arg):
        """
        Print current context.

        If the arg ends with "." only the keys of the object are shown

        >>> mytest > context urls.
        www
        backoffice

        >>> mytest > context urls
        www:
            host: example.com
        backoffice:
            host: admin.example.com

        Alias: ctx
        """
        if arg.strip() == "." or not arg:
            for k in self.ctx.data.keys():
                print(k)
            return
        if arg.endswith("."):
            var = arg[:-1]
            ctx_var = self.ctx
            for part in var.split("."):
                ctx_var = ctx_var[part]
            if isinstance(ctx_var, dict):
                for k in ctx_var.keys():
                    print(k)
            print(var + " is not a map.")
            return
        else:
            print(arg + ":")
            ctx_var = self.ctx
            for part in arg.split("."):
                ctx_var = ctx_var[part]
            for line in pyaml.dump(ctx_var).splitlines():
                print("  " + line)
            return

    do_ctx = do_context

    def do_exit(self, arg):
        "Exit the shell and abort the test"
        self.abort = True
        return True

    async def do_run(self, args):
        "Run the current task"
        self.task.interactive = False
        await self.task.__run__(self.ctx)
        self.task.interactive = True

    def do_edit(self, arg):
        "Opens $EDITOR to edit the current task"
        (fd, name) = mkstemp(suffix=".yml", text=True)
        file = os.fdopen(fd, "w")
        yamlloader_as_yaml(self.task, output=file, name=self.task.__taskname__)
        file.close()
        run([os.environ.get("EDITOR", "nano"), name])


class TestShell(TaskShell):
    def __init__(self, task, context, *args, response=None,
                 json=None, **kwargs):
        super().__init__(task, context, *args, **kwargs)
        self.url, self.qs, self.headers, self.json_body = task.get_parts(context)
        self.response = response
        self.json = json
        self.abort = False

    async def do_run(self, args):
        "Run the current task"
        await super().do_run(args)
        print(self.task._result.as_dict())
        self.json = self.task._result["response.json"]

    def do_url(self, arg):
        "Print URL"
        print(self.url)

    def do_qs(self, arg):
        "Print query_string"
        print(self.qs)

    def do_headers(self, arg):
        "Print headers"
        print(self.headers)

    def do_json_body(self, arg):
        "Print "
        print(self.json_body)

    def do_jq(self, pattern):
        """
        Run a jq script against the result JSON.
        """
        if self.json is None:
            print("No response yet. Call 'run' first.")
        from jq import jq
        try:
            pprint(jq(pattern).transform(self.json))
        except Exception as e:
            print(e)
