from restinpy.parser import YamlLoader


class ContextProcessorBase(type):
    registry = {}

    def __init__(cls, name, bases, attrs):
        if "__context_processor_name__" in attrs:
            ContextProcessorBase.registry[cls.__context_processor_name__] = cls


class ContextProcessor(YamlLoader, metaclass=ContextProcessorBase):
    async def __call__(self, response, ctx):
        raise NotImplementedError()


def context_processor_factory(data):
    context_processor_name, data = list(data.items())[0]
    if isinstance(data, list):
        return ContextProcessorBase.registry[context_processor_name](data)
    else:
        return ContextProcessorBase.registry[context_processor_name](**data)
