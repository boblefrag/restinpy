import urllib

import pyaml
from aiohttp.client_exceptions import ContentTypeError

from restinpy.task import YamlTask, Task
from restinpy.loader import load_suite
from restinpy.extractor import extractor_factory
from restinpy.validator import validator_factory
from restinpy.context_processor import context_processor_factory
from restinpy.shell import TestShell

from restinpy.parser import (YamlLoader, MappingField, StringField,
                             ListField, ChoiceField, BooleanField, ObjField)
from restinpy.builtins.fields import ContextValueField
from restinpy.builtins.validators import StatusValidator


class ExtractListField(ListField):
    def __init__(self, **kwargs):
        super().__init__(item_type=extractor_factory, **kwargs)


class ValidateListField(ListField):
    def __init__(self, **kwargs):
        super().__init__(item_type=validator_factory, **kwargs)


class URL(YamlLoader):
    host = StringField()
    path = StringField(default="/")
    port = StringField(default="")
    scheme = StringField()
    extends = ContextValueField(default=None)
    headers = MappingField()
    query_string = MappingField()

    def build(self, ctx):
        if self.extends is not None:
            extended = ctx.get_path(ctx.render(self.extends))
            self.yl_update(extended)
        port = ":%s" % self.port if self.port else ""
        netloc = self.host + port
        pr = urllib.parse.ParseResult(self.scheme, netloc, self.path,
                                      [], [], [])
        return (ctx.render(pr.geturl()), self.build_qs(ctx),
                ctx.render(self.headers))

    def build_qs(self, ctx):
        # TODO: handle lists
        return [(k, ctx.render(v)) for k, v in self.query_string.items()]


class Context_(Task):
    __taskname__ = "context"

    def __init__(self, processors):
        super().__init__(processors)
        self.processors = processors

    async def __run__(self, ctx):
        for proc in self.processors:
            proc = context_processor_factory(proc)
            await proc(ctx)


class Message(YamlTask):
    message = StringField(default="")
    context = ListField(default=[])

    async def __run__(self, ctx):
        self._result.log(ctx.render(self.message))
        for var in self.context:
            self._result.log("  %s:" % var)
            ctx_var = ctx
            for part in var.split("."):
                ctx_var = ctx_var[part]
            for line in pyaml.dump(ctx_var).splitlines():
                self._result.log("    " + line)


EXPECTED_STATUS = {
    "POST": [200, 201, 204],
    "PATCH": [200, 201, 204],
    "PUT": [200, 201, 204],
    "DELETE": [200, 202, 204],
}


class Test(YamlTask):
    method = ChoiceField(["GET", "POST", "PUT", "DELETE", "OPTION", "HEAD"])
    url = ObjField(URL)
    json_body = MappingField(default=None)
    validate = ValidateListField(default=[])
    extract = ExtractListField(default=[])
    debug = BooleanField(default=False)
    interactive = BooleanField(default=False)

    def __init__(self, *args, **kwargs):
        self.__handlers__ = []
        super().__init__(*args, **kwargs)

    def get_parts(self, ctx):
        url, qs, headers = self.url.build(ctx)
        json_body = ctx.render(self.json_body)
        return url, qs, headers, json_body

    async def __run__(self, ctx):
        url, qs, headers, json_body = self.get_parts(ctx)

        if self.interactive:
            cli = TestShell(self, ctx)
            await cli.cmdloop()
            self._result = cli.task._result
            return

        self._result.log_debug((url, qs, headers))
        self._result.log_debug(json_body)

        self._result["request"] = dict(
            method=self.method,
            url=url,
            query_string=qs,
            headers=headers,
            json=json_body
        )
        sess = ctx["_http_session"]
        executor = getattr(sess, self.method.lower())
        async with executor(url, params=qs,
                            headers=headers, json=json_body) as resp:
            self._result.log_debug("response:")
            self._result.log_debug("  %s" % resp.status)
            self._result.log_debug("  %s" % await resp.text())
            await self.validate_response(ctx, resp)
            await self.extract_values(ctx, resp)
            try:
                self._result["response.json"] = await resp.json()
            except ContentTypeError:
                pass
            self._result["response.text"] = await resp.text()

    async def validate_response(self, ctx, response):
        got_status = False
        for v in self.validate:
            if v.__validator_name__ == "status":
                got_status = True
            await v(response, ctx)
        if not got_status:
            v = StatusValidator(
                    EXPECTED_STATUS.get(self.method.upper(), [200]))
            await v(response, ctx)

    async def extract_values(self, ctx, response):
        for e in self.extract:
            await e(response, ctx)


class Include(Task):
    def __init__(self, suitename):
        super().__init__(suitename)
        self.suitename = suitename

    async def __run__(self, ctx):
        s = await load_suite(self.suitename)
        return await s(ctx)


class Fork(Task):
    def __init__(self, suitename):
        super().__init__(suitename)
        self.suitename = suitename

    async def __run__(self, ctx):
        s = await load_suite(self.suitename)
        return s(ctx)
