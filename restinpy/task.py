import sys
import time
from asyncio import iscoroutine
from traceback import format_exception

from restinpy.parser import YamlLoader


class TaskBase(type):
    registry = {}

    def __init__(cls, name, bases, attrs):
        cls.__taskname__ = attrs.get("__taskname__", name.lower())
        TaskBase.registry[cls.__taskname__] = cls


class Task(metaclass=TaskBase):

    def __init__(self, *args, **kwargs):
        self._result = TaskResult(self)

    async def __run__(self, ctx):
        raise NotImplementedError()

    async def __call__(self, ctx):
        self._result.set_context(ctx)
        self._result.start()
        try:
            res = await self.__run__(ctx)
            if iscoroutine(res):
                self._result.stop()
                return res
        except Exception as e:
            self._result.log_exception(e, sys.exc_info()[2])
        self._result.stop()
        return self._result


class YamlTask(YamlLoader, Task):
    def __init__(self, *args, **kwargs):
        Task.__init__(self)
        YamlLoader.__init__(self, *args, **kwargs)


def task_factory(data):
    task_name, data = list(data.items())[0]
    try:
        cls = TaskBase.registry[task_name]
    except KeyError:
        raise ValueError("No Task named `%s` found" % task_name)
    if not isinstance(data, dict):
        return cls(data)
    else:
        return cls(**data)


class TaskResult:
    def __init__(self, task):
        self.result = {}
        self.errors = []
        self.warnings = []
        self.skipped = None
        self.taskname = None
        self.task = task
        self._ctx = None
        self._start = None
        self._stop = None

    @property
    def ctx(self):
        if self._ctx is None:
            raise ValueError("Call set_context(ctx) before using it.")
        return self._ctx

    def set_context(self, ctx):
        self._ctx = ctx

    def log(self, msg):
        self.ctx["_out"].write(str(msg) + "\n")

    def log_debug(self, msg):
        if self.ctx.get("_debug") or self.task.debug:
            self.ctx["_out"].write(str(msg) + "\n")

    def log_error(self, msg):
        self.errors.append(msg)

    def log_warning(self, msg):
        self.warnings.append(msg)

    def log_exception(self, e, tb=None):
        exc = format_exception(e, e, tb)
        for line in exc:
            self.log(line.rstrip())
        self.errors.append(exc)
        return

    def start(self):
        self._start = time.time()

    def stop(self):
        self._stop = time.time()

    def __setitem__(self, key, value):
        path = key.split(".")
        last = path.pop()
        result = self.result
        for k in path:
            result = result.setdefault(k, {})
        result[last] = value

    def __getitem__(self, key):
        path = key.split(".")
        last = path.pop()
        result = self.result
        for k in path:
            result = result[k]
        return result[last]

    @property
    def success(self):
        return not bool(self.errors)

    def as_dict(self):
        if self._start and self._stop:
            duration = self._stop - self._start
        else:
            duration = None
        return dict(
            errors=self.errors,
            warnings=self.warnings,
            skipped=bool(self.skipped),
            result=self.result,
            success=self.success,
            duration=duration,
            task_name=self.taskname,
            type=self.task.__taskname__,
        )

