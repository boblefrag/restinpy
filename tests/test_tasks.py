from io import StringIO

import pytest

from restinpy import init_plugins
from restinpy.tasks import Message, Context_, Test as Test_
from restinpy.context import Context


init_plugins()


@pytest.fixture
def context():
    data = """---
domain: example.com
content_type: application/json
foo: bar
fobar: baz
"""
    ctx = Context(data)
    ctx["_out"] = StringIO()
    return ctx


@pytest.mark.asyncio
async def test_message(context):
    m = Message(message="foo is {{ foo }}", context=["foo"])
    await m(context)
    expected_output = """foo is bar
  foo:
    bar
    ...
"""
    out = context["_out"].getvalue()
    assert out == expected_output


@pytest.mark.asyncio
async def test_context(context):
    c = Context_([{"copy":{"src": "foo", "dest": "baz"}}])
    await c(context)
    assert context["baz"] == "bar"


@pytest.mark.asyncio
async def test_test(context):
    t = Test_(
        method="GET",
        url={
            "scheme": "https",
            "host": "{{ domain }}",
            "port": 8000,
            "path": "{{ foo }}",
            "headers": {
                "Content-Type": "{{ content_type }}"
            }
        }
    )
    url, qs, headers = t.url.build(context)
    assert url == "https://example.com:8000/bar"
    assert qs == []
    assert headers == {"Content-Type": "application/json"}


def test_test2(context):
    t = Test_(
        method="GET",
        url={
            "scheme": "https",
            "host": "{{ domain }}",
            "port": 8000,
            "path": "{{ foo }}",
            "headers": {
                "Content-Type": "{{ content_type }}"
            }
        }
    )
    from restinpy.parser import yamlloader_as_yaml
    print(yamlloader_as_yaml(t, name="test", comments=True))
