import pytest
import yaml
from restinpy.parser import (
    YamlLoader, StringField, BooleanField, ListField, MappingField
)


def test_loader():
    class _Test(YamlLoader):
        string = StringField()
        boolean = BooleanField
    data = yaml.load(
        """---
string: foo
boolean: yes
"""
    )
    t = _Test(**data)
    assert t.string == "foo"
    assert t.boolean
    t.string = "bar"
    t.boolean = False
    assert t.string == "bar"
    assert not t.boolean
