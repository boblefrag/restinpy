import pytest

from restinpy.extractor import Extractor, extractor_factory


@pytest.mark.asyncio
async def test_extractor_base():
    class MyExtractor(Extractor):
        __extractor_name__ = "myextractor"

        async def __call__(self, data, ctx):
            return

    e = extractor_factory({"myextractor": {}})
    await e(1, 2)
