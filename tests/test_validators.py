import pytest

from restinpy.validator import Validator, ValidationError, validator_factory


@pytest.mark.asyncio
async def test_validator():
    class MyValidator(Validator):
        __validator_name__ = "myvalidator"
        async def __call__(self, data, ctx):
            return
    v = validator_factory({"myvalidator": {}})
    await v(1, 2)
