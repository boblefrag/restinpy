import os
import pytest
from restinpy.context import context_from_file, UndefinedError


HERE = os.path.dirname(__file__)


def test_load():
    with open(os.path.sep.join([HERE, "tests.yaml"])) as f:
        ctx = context_from_file(f)
    assert ctx['url']['base_url'] == "http://example.com"
    assert ctx['url']['url'] == "http://example.com/stuff"


def test_udpate():
    with open(os.path.sep.join([HERE, "tests.yaml"])) as f:
        ctx = context_from_file(f)

    # before applying overlay
    assert ctx['url']['scheme'] == "http"
    assert ctx['url']['query_string']['bar'] == "bar"
    with pytest.raises(KeyError):
        ctx['url']['foo']
    with pytest.raises(KeyError):
        ctx['url']['query_string']['fubar']

    # apply
    overlay = dict(
            url=dict(
                scheme="https",
                foo="bar",
                query_string=dict(
                    bar="baz",
                    fubar="fubar")))
    ctx.update(overlay)

    # after apply
    # change append
    assert ctx['url']['scheme'] == "https"
    # additionnal stuff was added
    assert ctx['url']['foo'] == "bar"
    # same for nested dicts
    assert ctx['url']['query_string']['bar'] == "baz"
    assert ctx['url']['query_string']['fubar'] == "fubar"


def test_overlay():
    with open(os.path.sep.join([HERE, "tests.yaml"])) as f:
        ctx = context_from_file(f)
    with open(os.path.sep.join([HERE, "overlay.yaml"])) as f:
        overlay = context_from_file(f)
    with pytest.raises(UndefinedError):
        ctx['user']['staff']['login']
    ctx.update(overlay)
    assert ctx['user']['staff']['login'] == "aa@bb.cc"


def test_undefined():
    with open(os.path.sep.join([HERE, "tests.yaml"])) as f:
        ctx = context_from_file(f)
    with pytest.raises(UndefinedError):
        ctx['user']['staff']['login']
    # ctx['user']['staff']['login'] = "staff user"
    # assert ctx['user']['staff']['login'] == "staff user"


def test_bind():
    with open(os.path.sep.join([HERE, "tests.yaml"])) as f:
        ctx = context_from_file(f)
    assert ctx['url']['base_url'] == "http://example.com"
    ctx.bind("url.base_url", "https://foo.bar")
    assert ctx['url']['base_url'] == "https://foo.bar"
    ctx.bind("url", "https://foo.bar")
    assert ctx['url'] == "https://foo.bar"

