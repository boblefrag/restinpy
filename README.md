[![pipeline status](https://gitlab.com/lisael/restinpy/badges/master/pipeline.svg)](https://gitlab.com/lisael/restinpy/commits/master)
[![coverage report](https://gitlab.com/lisael/restinpy/badges/master/coverage.svg)](https://gitlab.com/lisael/restinpy/commits/master)

# Restinpy

Declarative JSON-REST api test framework

## Features

- Declarative yaml syntax
- Ansible-like architecture and lazy jinja templating
- validator/extractor/context_processors
- Leverage asyncio (with aiohttp client) to parallelize runs
- Interactive test case builder

## Quick start

### Create a project

```sh
mkdir myproject
cd myproject
mkdir {suite,context}
```

### Add a context

```sh
$EDITOR context/base.yml
```

```yaml
# myproject/context/base.yml
---
gitlab_token: "{{ '' | undefined('Your gitlab token.')}}"

target_user: "lisael"
target_project_name: "restinpy"

domain: gitlab.com
default_scheme: https

urls:
  default:
    host: "{{ domain }}"
    scheme: "{{ default_scheme }}"
    headers:
      Content-Type: application/json
      PRIVATE-TOKEN: "{{ gitlab_token }}"
```

This defines a context that is available in all tests.

The `gitlab_token` entry is not defined yet. However the context is lazy.
Even not fully resolved it can be used as long as `gitlab_token` is not
queried (an `UndefinedError` is raised).

There are 3 ways to update this context:

1. Define another context that `extends` this one
2. Pass an `--extra-var` option at the invocation of the tests
3. Manipulate the context with `context_processors` defined in the tests.

### Add a test suite

```sh
$EDITOR suite/fetch_project.yml
```

```yaml
---
- test:
    name: Get user
    method: GET
    debug: no
    url:
      extends: urls.default
      path: /api/v4/users
      query_string:
        username: "{{ target_user }}"
```

The URL is not fully defined here, as it extends the context variable `url.default`.
Only the API endpoint (aka. `path`) and the query_string are required.

### Run the tests

```sh
export GITLAB_TOKEN=XXXXXXXXXXXX
restinpy base fetch_project --extra_var="gitlab_token:$GITLAB_TOKEN"
```
